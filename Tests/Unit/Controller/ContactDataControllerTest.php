<?php
namespace HIVE\HiveExtContactdata\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Perrin Ennen <p.ennen@teufels.com>
 */
class ContactDataControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtContactdata\Controller\ContactDataController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\HIVE\HiveExtContactdata\Controller\ContactDataController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllContactDatasFromRepositoryAndAssignsThemToView()
    {

        $allContactDatas = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $contactDataRepository = $this->getMockBuilder(\HIVE\HiveExtContactdata\Domain\Repository\ContactDataRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $contactDataRepository->expects(self::once())->method('findAll')->will(self::returnValue($allContactDatas));
        $this->inject($this->subject, 'contactDataRepository', $contactDataRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('contactDatas', $allContactDatas);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
