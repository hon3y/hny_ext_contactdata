<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtContactdata',
            'Hivecontactdatalistcontactdata',
            [
                'ContactData' => 'list'
            ],
            // non-cacheable actions
            [
                'ContactData' => 'list'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecontactdatalistcontactdata {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_contactdata') . 'Resources/Public/Icons/user_plugin_hivecontactdatalistcontactdata.svg
                        title = LLL:EXT:hive_ext_contactdata/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_contactdata_domain_model_hivecontactdatalistcontactdata
                        description = LLL:EXT:hive_ext_contactdata/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_contactdata_domain_model_hivecontactdatalistcontactdata.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextcontactdata_hivecontactdatalistcontactdata
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
