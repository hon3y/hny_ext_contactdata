
plugin.tx_hiveextcontactdata_hivecontactdatalistcontactdata {
    view {
        # cat=plugin.tx_hiveextcontactdata_hivecontactdatalistcontactdata/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_contactdata/Resources/Private/Templates/
        # cat=plugin.tx_hiveextcontactdata_hivecontactdatalistcontactdata/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_contactdata/Resources/Private/Partials/
        # cat=plugin.tx_hiveextcontactdata_hivecontactdatalistcontactdata/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_contactdata/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextcontactdata_hivecontactdatalistcontactdata//a; type=string; label=Default storage PID
        storagePid =
    }
}
