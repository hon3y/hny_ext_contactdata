<?php
namespace HIVE\HiveExtContactdata\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_contactdata" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *
 ***/

/**
 * ContactData
 */
class ContactData extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * backendtitle
     *
     * @var string
     * @validate NotEmpty
     */
    protected $backendtitle = '';

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';

    /**
     * fax
     *
     * @var string
     */
    protected $fax = '';

    /**
     * mobile
     *
     * @var string
     */
    protected $mobile = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * website
     *
     * @var string
     */
    protected $website = '';

    /**
     * facebook
     *
     * @var string
     */
    protected $facebook = '';

    /**
     * twitter
     *
     * @var string
     */
    protected $twitter = '';

    /**
     * googleplus
     *
     * @var string
     */
    protected $googleplus = '';

    /**
     * linkedin
     *
     * @var string
     */
    protected $linkedin = '';

    /**
     * instagram
     *
     * @var string
     */
    protected $instagram = '';

    /**
     * xing
     *
     * @var string
     */
    protected $xing = '';

    /**
     * wechat
     *
     * @var string
     */
    protected $wechat = '';

    /**
     * skype
     *
     * @var string
     */
    protected $skype = '';

    /**
     * youtube
     *
     * @var string
     */
    protected $youtube = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * Returns the backendtitle
     *
     * @return string $backendtitle
     */
    public function getBackendtitle()
    {
        return $this->backendtitle;
    }

    /**
     * Sets the backendtitle
     *
     * @param string $backendtitle
     * @return void
     */
    public function setBackendtitle($backendtitle)
    {
        $this->backendtitle = $backendtitle;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Returns the fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Returns the mobile
     *
     * @return string $mobile
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Sets the mobile
     *
     * @param string $mobile
     * @return void
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the website
     *
     * @return string $website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Sets the website
     *
     * @param string $website
     * @return void
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * Returns the facebook
     *
     * @return string $facebook
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Sets the facebook
     *
     * @param string $facebook
     * @return void
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * Returns the twitter
     *
     * @return string $twitter
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Sets the twitter
     *
     * @param string $twitter
     * @return void
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * Returns the googleplus
     *
     * @return string $googleplus
     */
    public function getGoogleplus()
    {
        return $this->googleplus;
    }

    /**
     * Sets the googleplus
     *
     * @param string $googleplus
     * @return void
     */
    public function setGoogleplus($googleplus)
    {
        $this->googleplus = $googleplus;
    }

    /**
     * Returns the linkedin
     *
     * @return string $linkedin
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * Sets the linkedin
     *
     * @param string $linkedin
     * @return void
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;
    }

    /**
     * Returns the instagram
     *
     * @return string $instagram
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * Sets the instagram
     *
     * @param string $instagram
     * @return void
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;
    }

    /**
     * Returns the xing
     *
     * @return string $xing
     */
    public function getXing()
    {
        return $this->xing;
    }

    /**
     * Sets the xing
     *
     * @param string $xing
     * @return void
     */
    public function setXing($xing)
    {
        $this->xing = $xing;
    }

    /**
     * Returns the wechat
     *
     * @return string $wechat
     */
    public function getWechat()
    {
        return $this->wechat;
    }

    /**
     * Sets the wechat
     *
     * @param string $wechat
     * @return void
     */
    public function setWechat($wechat)
    {
        $this->wechat = $wechat;
    }

    /**
     * Returns the skype
     *
     * @return string $skype
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Sets the skype
     *
     * @param string $skype
     * @return void
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
    }

    /**
     * Returns the youtube
     *
     * @return string $youtube
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Sets the youtube
     *
     * @param string $youtube
     * @return void
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}
